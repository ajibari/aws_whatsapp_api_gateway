'use strict';

module.exports = (sequelize, DataTypes) => {
  const Wa_device = sequelize.define('wa_device', {
  	id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    client_code: {
      	type: Sequelize.STRING,
      	allowNull: true,
    },
    device_code: {
    	type: Sequelize.STRING,
    	allowNull: true,
    },
    created_on: {
    	type: Sequelize.DATE
    },
    updated_on: {
    	type: Sequelize.DATE
    }
  },{

      freezeTableName: true,
      tableName: 'wa_device',
  
  });

  Wa_device.associate = (models) => {
    Wa_device.hasOne(models.Wa_auth, {
      foreignKey: 'client_code',
      as: 'Wa_deviceClient_code',
    });

    Wa_device.hasMany(models.Wa_notification, {
      foreignKey: 'client_code',
      as: 'Wa_deviceClient_code2',
    });
  };

  return Wa_device;
};