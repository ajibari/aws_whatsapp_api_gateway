'use strict';

module.exports = (sequelize, DataTypes) => {
  const Wa_auth = sequelize.define('wa_auth', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    username: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    key: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    created_on: {
        type: DataTypes.DATE
    },
    updated_on: {
        type: DataTypes.DATE
    },
    active: {
        type: DataTypes.INTEGER,
        defaultValue: 1
    }
  },{

      freezeTableName: true,
      tableName: 'wa_auth',
  
  });

  Wa_auth.associate = (models) => {
    Wa_auth.hasOne(models.Wa_device, {
      foreignKey: 'username',
      as: 'Wa_authUsername',
    });
  };

  return Wa_auth;
};