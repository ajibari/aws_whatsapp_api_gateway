'use strict';

module.exports = (sequelize, DataTypes) => {
  const Wa_notification = sequelize.define('wa_notification', {
  	wa_notification_id: {
        field: 'wa_notification_id',
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    client_code: {
        field: 'client_code',
      	type: DataTypes.STRING,
      	allowNull: true,
    },
    phone: {
        field: 'phone',
      	type: DataTypes.STRING,
      	allowNull: true,
    },
    data: {
        field: 'data',
        type: DataTypes.TEXT
    },
    type: {
        field: 'type',
        type: DataTypes.STRING
    },
    device_code: {
        field: 'device_code',
        type: DataTypes.STRING
    },
    status: {
        field: 'status',
        type: DataTypes.STRING
    },
    created_on: {
        field: 'created_on',
        type: DataTypes.DATE
    },
    updated_on: {
        field: 'updated_on',
        type: DataTypes.DATE
    }
  },{

      freezeTableName: true,
      tableName: 'wa_notification',
  
  });

  Wa_notification.associate = (models) => {
    Wa_notification.hasOne(models.Wa_device, {
      foreignKey: 'client_code',
      as: 'Wa_notificationClient_code',
    });
  };

  return Wa_notification;
};