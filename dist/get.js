"use strict";

var db = require('./config/db');
var wa_notification = require('./models/wa_notification')(db.sequelize, db.Sequelize);
var wa_auth = require('./models/wa_auth')(db.sequelize, db.Sequelize);

module.exports.get = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    var result = {}, dataJson, wa_notification_id = [];
    var header = event.headers;

    checkHeader(header)
    .then(function(response){
        
        if(!response){
            var status = {
                status: "0",
                message: "Authentication is not valid"
            }

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(status)
            };
            callback(null, response);
            return;
        }

        return wa_notification.sequelize.transaction(function(t1) {
            return wa_notification.findAll({ 
                order: [
                    ['wa_notification_id', 'ASC']
                ],
                limit: parseInt(event.pathParameters.number_of_message),
                where: {
                        device_code:  event.pathParameters.device_id,
                        status: '0'
                },
                transaction: t1,
                
                logging: console.log
            }, {transaction: t1}).then(function(response) {
                for(var i = 0, len = response.length; i < len; i++){
                    wa_notification_id.push(response[i].wa_notification_id);
                }

                if(response){
                    
                    return wa_notification.update({
                      status: '1',
                    }, {
                      where: {
                        wa_notification_id: wa_notification_id
                      }
                    })
                    .then(function(){
                        result.success = 1;
                        result.data = [];

                        for (var i = 0, len = response.length; i < len; i++) {
                            dataJson = JSON.parse(response[i].data);
                            result.data.push({
                                phone: response[i].phone,
                                message: "Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code,
                                id: response[i].wa_notification_id
                            })
                        }
                        
                        return response;
                    })
                    .catch(function(err){
                        var responseError = {
                            statusCode: 200, 
                            headers: jsonResponseHeaders,
                            body: JSON.stringify("Error updating status message")
                        }
                        callback(null, responseError);
                    })

                }
            }, {transaction: t1}).then(function(){
                
                var response = {
                    statusCode: 200,
                    headers: jsonResponseHeaders,
                    body: JSON.stringify(result)
                };

                callback(null, response);
            })
        })



    /*
        wa_notification.findAll({ 
            order: [
                ['wa_notification_id', 'ASC']
            ],
            limit: parseInt(event.pathParameters.number_of_message),
            where: {
                    device_code:  event.pathParameters.device_id,
                    status: '0'
            },
        })
        .then(function (response) {

            result.success = 1;
            result.data = [];

            for (var i = 0, len = response.length; i < len; i++) {
                dataJson = JSON.parse(response[i].data);
                result.data.push({
                    phone: response[i].phone,
                    message: "Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code,
                    id: response[i].wa_notification_id
                })
            }
            
            return response;
        })
        .then(function(response){

            var wa_notification_id = [];

            for(var i = 0, len = response.length; i < len; i++){
                wa_notification_id.push(response[i].wa_notification_id);
            }

            //update data to 1

                wa_notification.update({
                  status: '1',
                }, {
                  where: {
                    wa_notification_id: wa_notification_id
                  }
                })
                .catch(function(err){
                    var responseError = {
                        statusCode: 200, 
                        headers: jsonResponseHeaders,
                        body: JSON.stringify("Error updating status message")
                    }
                    callback(null, responseError);
                })

                return true;
        })
        .then(function(res){

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(result)
            };

            callback(null, response);
        })
        .catch(function (error) {
            console.error(error);
            callback(null, {
                statusCode: 501,
                headers: textResponseHeaders,
                body: "Couldn't find the order, Error finding from DB, Error: " + error
            });
        });
    */

    })
    .catch(function(err){
        console.log(err)
    })

};

module.exports.get_failed = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    var result = {}, dataJson, idMessageFail = [];
    var header = event.headers;

    checkHeader(header)
    .then(function(response){

        if(!response){
            var status = {
                status: "0",
                message: "Authentication is not valid"
            }

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(status)
            };
            callback(null, response);
            return;
        }

        return wa_notification.sequelize.transaction(function(t1) {
            return wa_notification.findAll({ 
                order: [
                    ['wa_notification_id', 'ASC']
                ],
                limit: parseInt(event.pathParameters.number_of_message),
                where: {
                        device_code:  event.pathParameters.device_id,
                        status: '-1'
                },
                transaction: t1,
                
                logging: console.log
            }, {transaction: t1}).then(function(response) {
                for(var i = 0, len = response.length; i < len; i++){
                    wa_notification_id.push(response[i].wa_notification_id);
                }

                if(response){
                    
                    return wa_notification.update({
                      status: '3',
                    }, {
                      where: {
                        wa_notification_id: wa_notification_id
                      }
                    })
                    .then(function(){
                        result.success = 1;
                        result.data = [];

                        for (var i = 0, len = response.length; i < len; i++) {
                            dataJson = JSON.parse(response[i].data);
                            result.data.push({
                                phone: response[i].phone,
                                message: "Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code,
                                id: response[i].wa_notification_id
                            })
                        }
                        
                        return response;
                    })
                    .catch(function(err){
                        var responseError = {
                            statusCode: 200, 
                            headers: jsonResponseHeaders,
                            body: JSON.stringify("Error updating status message")
                        }
                        callback(null, responseError);
                    })

                }
            }, {transaction: t1}).then(function(){
                
                var response = {
                    statusCode: 200,
                    headers: jsonResponseHeaders,
                    body: JSON.stringify(result)
                };

                callback(null, response);
            })
        })

    })
    .catch(function(err){
        console.log(err)
    })

    /*
        wa_notification.findAll({ 
            order: [
                ['wa_notification_id', 'ASC']
            ],
            limit: parseInt(event.pathParameters.number_of_message),
            where: {
                    device_code:  event.pathParameters.device_id,
                    status: '-1'  
            }
        })
        .then(function (response) {
            for (var i = 0, len = response.length; i < len; i++) {
                idMessageFail.push(response[i].wa_notification_id)
            }

            wa_notification.update({
                status: '3',
            }, {
                where: {
                    wa_notification_id: idMessageFail
                }
            })

            return response;
        })
        .then(function (response) {

            result.success = 1;
            result.data = [];

            for (var i = 0, len = response.length; i < len; i++) {
                dataJson = JSON.parse(response[i].data);
                result.data.push({
                    phone: response[i].phone,
                    message: "Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code,
                    id: response[i].wa_notification_id
                })
            }
            
            return result;
        })
        .then(function(result){

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(result)
            };

            callback(null, response);
        })
    */
};

async function checkHeader(headers, callback){
    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    return wa_auth.findOne({ 
                where: {
                    username:  headers.x_auth_username,
                    key: headers.x_auth_key
                },
            })
            .then(function(response){
                if(response){
                    return true;
                }
                else{
                    return false;
                }
            })
            .catch(function(err){
                console.log(err);
                return false;
            });
}