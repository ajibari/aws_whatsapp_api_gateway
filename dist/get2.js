"use strict";

var db = require('./config/db');
var wa_notification = require('./models/wa_notification')(db.sequelize, db.Sequelize);

module.exports.get = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    var result = [], dataJson;

    wa_notification.findAll({ 
        order: [
            ['wa_notification_id', 'ASC']
        ],
        limit: parseInt(event.pathParameters.number_of_message),
        where: {
                device_code:  event.pathParameters.device_id,
                status: 0  
        },
        
      }).then(function (response) {

        for (var i = 0, len = response.length; i < len; i++) {
            dataJson = JSON.parse(response[i].data);
            result.push({
                success: 1,
                data: [{
                    phone: response[i].phone,
                    message: "Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code,
                    id: response[i].wa_notification_id
                }]
            })
        }

        var response = {
            statusCode: 200,
            headers: jsonResponseHeaders,
            body: JSON.stringify(result)
        };
        callback(null, response);
    }).catch(function (error) {
        console.error(error);
        callback(null, {
            statusCode: 501,
            headers: textResponseHeaders,
            body: "Couldn't find the order, Error finding from DB, Error: " + error
        });
    });
};