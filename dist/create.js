"use strict";

var db = require('./config/db.js');
var wa_notification = require('./models/wa_notification')(db.sequelize, db.Sequelize);
var wa_auth = require('./models/wa_auth')(db.sequelize, db.Sequelize);
var Ajv = require('ajv');
var ajv = Ajv({ allErrors: true });
var querystring = require('querystring');
var waNotificationSchema = require('./validation/create-notif-validation.json');

module.exports.create = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };
    
    var data = querystring.parse(event.body);
    var header = event.headers;


    checkHeader(header)
    .then(function(response){
        
        if(!response){
            var status = {
                status: "0",
                message: "Authentication is not valid"
            }

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(status)
            };
            callback(null, response);
            return;
        }

        var valid = ajv.validate(waNotificationSchema, data);
        if (!valid) {
            console.error("Validation Failed");
            callback(null, {
                statusCode: 400,
                headers: jsonResponseHeaders,
                body: errorResponse(ajv.errors)
            });
            return;
        }

        data.status = 0;
        
        wa_notification.create(data).then(function () {
            var status = {
                status: 1,
                message: "Data created"
            }

            var response = {
                statusCode: 201,
                headers: jsonResponseHeaders,
                body: JSON.stringify(status)
            };
            callback(null, response);
        }).catch(function (error) {
            console.error(error);
            callback(null, {
                statusCode: error.statusCode || 501,
                headers: textResponseHeaders,
                body: "Couldn't create the order, Error inserting into DB, Error: " + error
            });
        });
    })
    .catch(function(err){
        console.log(err)
    })

};

function errorResponse(schemaErrors) {
    var errors = schemaErrors.map(function (error) {
        return {
            path: error.dataPath,
            message: error.message
        };
    });
    return {
        status: 'failed',
        errors: errors
    };
}

async function checkHeader(headers, callback){
    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    return wa_auth.findOne({ 
                where: {
                    username:  headers.x_auth_username,
                    key: headers.x_auth_key
                },
            })
            .then(function(response){
                if(response){
                    return true;
                }
                else{
                    return false;
                }
            })
            .catch(function(err){
                console.log(err);
                return false;
            });
}