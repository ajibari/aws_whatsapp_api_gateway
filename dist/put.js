"use strict";

var db = require('./config/db.js');
var wa_notification = require('./models/wa_notification')(db.sequelize, db.Sequelize);
var wa_auth = require('./models/wa_auth')(db.sequelize, db.Sequelize);
var querystring = require('querystring');
var http = require('http');
var db_mode = process.env.NODE_ENV || 'development';
var env = require('./config/env.json')[db_mode];
var rp = require('request-promise');
var request = require('request');

module.exports.put = function (event, context, callback) {
    context.callbackWaitsForEmptyEventLoop = false;

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    var result = {}, dataRaw, data;
    var header = event.headers;

    checkHeader(header)
    .then(function(response){
    	
    	if(!response){
            var status = {
                status: "0",
                message: "Authentication is not valid"
            }

            var response = {
                statusCode: 200,
                headers: jsonResponseHeaders,
                body: JSON.stringify(status)
            };
            callback(null, response);
            return;
        }

        dataRaw = querystring.parse(event.body);

		if(typeof dataRaw.data_success != "undefined"){
			
			data = JSON.parse(dataRaw.data_success);

			result = {
				success: 1,
				message: "update success"
			}

			wa_notification.update({
			  status: '2',
			}, {
			  where: {
			    wa_notification_id: data
			  }
			}).then(function (response) {
				var response = {
			            statusCode: 200,
			            headers: jsonResponseHeaders,
			            body: JSON.stringify(result)
			        };
			    
			    callback(null, response);	
			})
			
		}

		else{

			data = JSON.parse(dataRaw.data_fail);

			switch(response.username.toLowerCase()){
				case 'paket': 
					console.log('sending sms');
					sendSms(data, result, callback);
					break;
				default:
					updateStatusFailedWa(data, result, callback);
					break;
			}
		}
    })
    .catch(function(err){
    	console.log(err)
    })
}

function sendSms(data, result, callback){

    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    var options = {
    	host: env.host_sms,
    	port: env.host_sms_port,
    	method: 'GET'
    }, req, message, dataJson, statusNotif, responseHtml;

    async function asyncForEach(data, callback) {
      for (let index = 0; index < data.length; index++) {
        await callback(data[index], index, data)
      }
    }

    asyncForEach(data, async (result) => {
    	
    	//build text message
    		wa_notification.findOne({ 
		        where: {
		            wa_notification_id:  result,
		        },
		        
		    })
		    .then(function(response){
            	
            	dataJson = JSON.parse(response.data);

		    	message = querystring.escape("Hi "+ dataJson.receiver_name + ", " + dataJson.sender_name + " telah mengirimkan paket anda, Anda dapat melihat status paket anda di paket.id/is/" + dataJson.booking_code);

		    	options.path = '/sms/smsreguler.php?username=' + env.username_rajasms + '&password=' + env.password_rajasms + '&key=' + env.key_rajasms + '&number=' + response.phone + '&message=' + message;

		    	rp(options.host+options.path)
			    .then(function (html) {
			  		console.log('html ', html);
			        responseHtml = html.split('|');

			        switch(responseHtml[0]){
			        	case '0': 
			        		statusNotif = '3';
			        		break;
			        	default:
			        		statusNotif = '-2';
			        		break;

			        }

			        wa_notification.update({
					  status: statusNotif,
					}, {
					  where: {
					    wa_notification_id: response.wa_notification_id
					  }
					})

			    })
			    .catch(function (err) {
			        console.log(err)
			    });
		    })
		    .catch(function(error){
		    	console.log(error)
		    })

  	});

    return new Promise(function(){
    	var responseSMS = {
			success: 1,
			message: "update success"
		}

		var response = {
            statusCode: 200,
            headers: jsonResponseHeaders,
            body: JSON.stringify(responseSMS)
        };
		    
		callback(null, response);	
    })

}

function updateStatusFailedWa(data, result, callback){
	
    var textResponseHeaders = {
        'Content-Type': 'text/plain'
    };

    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };


	result = {
		success: 1,
		message: "update success"
	}

	wa_notification.update({
	  status: '-1',
	}, {
	  where: {
	    wa_notification_id: data
	  }
	})
	.then(function (response) {
		var response = {
	            statusCode: 200,
	            headers: jsonResponseHeaders,
	            body: JSON.stringify(result)
	        };
	    
	    callback(null, response);	
	})

}

async function checkHeader(headers, callback){
    var jsonResponseHeaders = {
        'Content-Type': 'application/json'
    };

    return wa_auth.findOne({ 
                where: {
                    username:  headers.x_auth_username,
                    key: headers.x_auth_key
                },
            })
            .then(function(response){
                if(response){
                    return response;
                }
                else{
                    return false;
                }
            })
            .catch(function(err){
                console.log(err);
                return false;
            });
}